pragma solidity 0.5.8;

import "./oz/Ownable.sol";
import "./oz/IERC20.sol";
import "./oz/SafeMath.sol";

contract GuildBank is Ownable {
    using SafeMath for uint256;

    IERC20 public approvedToken; // approved token contract reference
    IERC20 public claimsToken; // claims token contract reference

    event Withdrawal(address indexed receiver, uint256 amount);
    event Funded(address indexed receiver, uint256 amount);

    constructor(address approvedTokenAddress, address claimsTokenAddress) public {
        approvedToken = IERC20(approvedTokenAddress);
        claimsToken = IERC20(claimsTokenAddress);
    }

    function withdraw(address receiver, uint256 shares) public onlyOwner returns (bool) {
        emit Withdrawal(receiver, shares);
        return claimsToken.transfer(receiver, shares);
    }

    function fund(address receiver, uint256 fundingAmount) public onlyOwner returns (bool) {
        emit Funded(receiver, fundingAmount);
        return approvedToken.transfer(receiver, fundingAmount);
    }
}